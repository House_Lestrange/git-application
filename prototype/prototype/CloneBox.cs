﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace prototype
{
    public partial class CloneBox : Form
    {
        GIT_BL BL = new GIT_BL();
        public CloneBox()
        {
            InitializeComponent();
           
            
            if (BL.get_GIT_Link() != null)
            {
                // BL.set_GIT_Link(git_link);
                Link_Txt.Text = BL.get_GIT_Link();

            }
            if (BL.get_Local_clone_dir() != null)
            {
                //BL.set_Local_clone_dir(local_clone_dir);
                Local_Dir_txt.Text = BL.get_Local_clone_dir();

            }

        }

        private void CloneBtn_Click(object sender, EventArgs e)
        {

            CloneResult.Text = string.Empty;
            string git_link = Link_Txt.Text.ToString();
            string local_clone_dir = Local_Dir_txt.Text.ToString();

            if (BL.get_GIT_Link() == "" )
            {
                BL.set_GIT_Link(git_link);
            }
            if(BL.get_Local_clone_dir() == "" )
            {
                BL.set_Local_clone_dir(local_clone_dir);
            }
            //if new link is given reset the global link
            if(BL.get_GIT_Link().ToString().Trim(' ') != git_link.Trim(' '))
            {
                BL.set_GIT_Link(git_link);
            }
            //if new local clone dir is given reset global clone directory
            if (BL.get_Local_clone_dir().ToString().Trim(' ') != local_clone_dir.Trim(' '))
            {
                BL.set_Local_clone_dir(local_clone_dir);
            }
            string res = BL.Clone();
            CloneResult.Text = res;
        }

        private void label2_Click(object sender, EventArgs e)
        {
            string path = Local_Dir_txt.Text.ToString();
            BL.FileViewer(ref path);
            Local_Dir_txt.Text =path;
            BL.set_Local_clone_dir(path);
        }
        private void do_when_close(object sender, EventArgs e)
        {

        }
    }
}
