﻿namespace prototype
{
    partial class GitControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GitControl));
            this.Tootls = new System.Windows.Forms.MenuStrip();
            this.OpenCloneBox = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RefreshTool = new System.Windows.Forms.ToolStripMenuItem();
            this.Management = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.EmulatorPanel = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.CmdTxt = new System.Windows.Forms.TextBox();
            this.ExecuteBtn = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.EmulatorResult = new System.Windows.Forms.RichTextBox();
            this.Local_repo_dir_txt = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Local_dir_txt = new System.Windows.Forms.TextBox();
            this.GIT_Link_txt = new System.Windows.Forms.TextBox();
            this.GIT_dir_txt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.RevertBtn = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.RevertTxt = new System.Windows.Forms.TextBox();
            this.commitTxt = new System.Windows.Forms.TextBox();
            this.addText = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.PushBtn = new System.Windows.Forms.Button();
            this.CommitBtn = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.AddBtn = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.StagedCount = new System.Windows.Forms.Label();
            this.UnstageCount = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.DataGridStaged = new System.Windows.Forms.DataGridView();
            this.DataGridUnstaged = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.branchlbl = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.UpstreamTxt = new System.Windows.Forms.RichTextBox();
            this.DownstreamTxt = new System.Windows.Forms.RichTextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.repoNameTxt = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.Tootls.SuspendLayout();
            this.Management.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.EmulatorPanel.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridStaged)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridUnstaged)).BeginInit();
            this.panel2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // Tootls
            // 
            this.Tootls.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Tootls.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OpenCloneBox,
            this.settingsToolStripMenuItem,
            this.RefreshTool});
            this.Tootls.Location = new System.Drawing.Point(0, 0);
            this.Tootls.Name = "Tootls";
            this.Tootls.Size = new System.Drawing.Size(810, 24);
            this.Tootls.TabIndex = 1;
            this.Tootls.Text = "menuStrip1";
            // 
            // OpenCloneBox
            // 
            this.OpenCloneBox.Name = "OpenCloneBox";
            this.OpenCloneBox.Size = new System.Drawing.Size(50, 20);
            this.OpenCloneBox.Text = "Clone";
            this.OpenCloneBox.Click += new System.EventHandler(this.OpenCloneBox_Click);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.settingsToolStripMenuItem.Text = "Settings";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // RefreshTool
            // 
            this.RefreshTool.Name = "RefreshTool";
            this.RefreshTool.Size = new System.Drawing.Size(58, 20);
            this.RefreshTool.Text = "Refresh";
            this.RefreshTool.Click += new System.EventHandler(this.RefreshTool_Click);
            // 
            // Management
            // 
            this.Management.Controls.Add(this.tabPage1);
            this.Management.Controls.Add(this.tabPage2);
            this.Management.Controls.Add(this.tabPage3);
            this.Management.Font = new System.Drawing.Font("Microsoft PhagsPa", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Management.Location = new System.Drawing.Point(13, 43);
            this.Management.Name = "Management";
            this.Management.SelectedIndex = 0;
            this.Management.Size = new System.Drawing.Size(785, 395);
            this.Management.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.White;
            this.tabPage1.Controls.Add(this.EmulatorPanel);
            this.tabPage1.Controls.Add(this.Local_repo_dir_txt);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.Local_dir_txt);
            this.tabPage1.Controls.Add(this.GIT_Link_txt);
            this.tabPage1.Controls.Add(this.GIT_dir_txt);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(777, 366);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Home";
            // 
            // EmulatorPanel
            // 
            this.EmulatorPanel.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.EmulatorPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.EmulatorPanel.Controls.Add(this.label8);
            this.EmulatorPanel.Controls.Add(this.CmdTxt);
            this.EmulatorPanel.Controls.Add(this.ExecuteBtn);
            this.EmulatorPanel.Controls.Add(this.label7);
            this.EmulatorPanel.Controls.Add(this.label6);
            this.EmulatorPanel.Controls.Add(this.EmulatorResult);
            this.EmulatorPanel.Location = new System.Drawing.Point(311, 22);
            this.EmulatorPanel.Name = "EmulatorPanel";
            this.EmulatorPanel.Size = new System.Drawing.Size(451, 301);
            this.EmulatorPanel.TabIndex = 17;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(30, 118);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 16);
            this.label8.TabIndex = 5;
            this.label8.Text = "Result :";
            // 
            // CmdTxt
            // 
            this.CmdTxt.Location = new System.Drawing.Point(90, 65);
            this.CmdTxt.Name = "CmdTxt";
            this.CmdTxt.Size = new System.Drawing.Size(245, 23);
            this.CmdTxt.TabIndex = 4;
            // 
            // ExecuteBtn
            // 
            this.ExecuteBtn.Location = new System.Drawing.Point(341, 63);
            this.ExecuteBtn.Name = "ExecuteBtn";
            this.ExecuteBtn.Size = new System.Drawing.Size(75, 23);
            this.ExecuteBtn.TabIndex = 3;
            this.ExecuteBtn.Text = "Execute";
            this.ExecuteBtn.UseVisualStyleBackColor = true;
            this.ExecuteBtn.Click += new System.EventHandler(this.ExecuteBtn_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(30, 68);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 16);
            this.label7.TabIndex = 2;
            this.label7.Text = "Command :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(142, 30);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(163, 16);
            this.label6.TabIndex = 1;
            this.label6.Text = "GIT Terminal Emulator";
            // 
            // EmulatorResult
            // 
            this.EmulatorResult.Location = new System.Drawing.Point(33, 134);
            this.EmulatorResult.Name = "EmulatorResult";
            this.EmulatorResult.Size = new System.Drawing.Size(383, 143);
            this.EmulatorResult.TabIndex = 0;
            this.EmulatorResult.Text = "";
            // 
            // Local_repo_dir_txt
            // 
            this.Local_repo_dir_txt.Location = new System.Drawing.Point(19, 254);
            this.Local_repo_dir_txt.Name = "Local_repo_dir_txt";
            this.Local_repo_dir_txt.ReadOnly = true;
            this.Local_repo_dir_txt.Size = new System.Drawing.Size(257, 23);
            this.Local_repo_dir_txt.TabIndex = 16;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 236);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(148, 16);
            this.label5.TabIndex = 15;
            this.label5.Text = "Local repository directory :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(83, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(147, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "GLOBAL PARAMETERS ";
            // 
            // Local_dir_txt
            // 
            this.Local_dir_txt.Location = new System.Drawing.Point(19, 204);
            this.Local_dir_txt.Name = "Local_dir_txt";
            this.Local_dir_txt.ReadOnly = true;
            this.Local_dir_txt.Size = new System.Drawing.Size(257, 23);
            this.Local_dir_txt.TabIndex = 11;
            // 
            // GIT_Link_txt
            // 
            this.GIT_Link_txt.Location = new System.Drawing.Point(19, 156);
            this.GIT_Link_txt.Name = "GIT_Link_txt";
            this.GIT_Link_txt.ReadOnly = true;
            this.GIT_Link_txt.Size = new System.Drawing.Size(257, 23);
            this.GIT_Link_txt.TabIndex = 10;
            // 
            // GIT_dir_txt
            // 
            this.GIT_dir_txt.Location = new System.Drawing.Point(19, 106);
            this.GIT_dir_txt.Name = "GIT_dir_txt";
            this.GIT_dir_txt.ReadOnly = true;
            this.GIT_dir_txt.Size = new System.Drawing.Size(257, 23);
            this.GIT_dir_txt.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 186);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(124, 16);
            this.label3.TabIndex = 8;
            this.label3.Text = "Local clone directory :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 139);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 16);
            this.label2.TabIndex = 7;
            this.label2.Text = "Git link repository :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 90);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 16);
            this.label1.TabIndex = 6;
            this.label1.Text = "GIT directory :";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.White;
            this.tabPage2.Controls.Add(this.label20);
            this.tabPage2.Controls.Add(this.RevertBtn);
            this.tabPage2.Controls.Add(this.label15);
            this.tabPage2.Controls.Add(this.label16);
            this.tabPage2.Controls.Add(this.RevertTxt);
            this.tabPage2.Controls.Add(this.commitTxt);
            this.tabPage2.Controls.Add(this.addText);
            this.tabPage2.Controls.Add(this.label14);
            this.tabPage2.Controls.Add(this.PushBtn);
            this.tabPage2.Controls.Add(this.CommitBtn);
            this.tabPage2.Controls.Add(this.label17);
            this.tabPage2.Controls.Add(this.AddBtn);
            this.tabPage2.Controls.Add(this.label18);
            this.tabPage2.Controls.Add(this.panel1);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(777, 366);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Management";
            // 
            // RevertBtn
            // 
            this.RevertBtn.Location = new System.Drawing.Point(686, 156);
            this.RevertBtn.Name = "RevertBtn";
            this.RevertBtn.Size = new System.Drawing.Size(75, 23);
            this.RevertBtn.TabIndex = 41;
            this.RevertBtn.Text = "Revert";
            this.RevertBtn.UseVisualStyleBackColor = true;
            this.RevertBtn.Click += new System.EventHandler(this.RevertBtn_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(507, 138);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(138, 12);
            this.label15.TabIndex = 40;
            this.label15.Text = "(Select items in the staged table)";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(458, 136);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(51, 16);
            this.label16.TabIndex = 39;
            this.label16.Text = "Unstage";
            // 
            // RevertTxt
            // 
            this.RevertTxt.Location = new System.Drawing.Point(457, 156);
            this.RevertTxt.Name = "RevertTxt";
            this.RevertTxt.Size = new System.Drawing.Size(223, 23);
            this.RevertTxt.TabIndex = 38;
            // 
            // commitTxt
            // 
            this.commitTxt.Location = new System.Drawing.Point(457, 213);
            this.commitTxt.Name = "commitTxt";
            this.commitTxt.Size = new System.Drawing.Size(304, 23);
            this.commitTxt.TabIndex = 33;
            // 
            // addText
            // 
            this.addText.Location = new System.Drawing.Point(457, 99);
            this.addText.Name = "addText";
            this.addText.Size = new System.Drawing.Size(223, 23);
            this.addText.TabIndex = 31;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(501, 82);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(143, 12);
            this.label14.TabIndex = 37;
            this.label14.Text = "(Select items in the unstage table)";
            // 
            // PushBtn
            // 
            this.PushBtn.Location = new System.Drawing.Point(545, 242);
            this.PushBtn.Name = "PushBtn";
            this.PushBtn.Size = new System.Drawing.Size(75, 23);
            this.PushBtn.TabIndex = 36;
            this.PushBtn.Text = "Push";
            this.PushBtn.UseVisualStyleBackColor = true;
            this.PushBtn.Click += new System.EventHandler(this.PushBtn_Click);
            // 
            // CommitBtn
            // 
            this.CommitBtn.Location = new System.Drawing.Point(461, 242);
            this.CommitBtn.Name = "CommitBtn";
            this.CommitBtn.Size = new System.Drawing.Size(75, 23);
            this.CommitBtn.TabIndex = 35;
            this.CommitBtn.Text = "Commit";
            this.CommitBtn.UseVisualStyleBackColor = true;
            this.CommitBtn.Click += new System.EventHandler(this.CommitBtn_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(458, 193);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(101, 16);
            this.label17.TabIndex = 34;
            this.label17.Text = "Commit Message:";
            // 
            // AddBtn
            // 
            this.AddBtn.Location = new System.Drawing.Point(686, 98);
            this.AddBtn.Name = "AddBtn";
            this.AddBtn.Size = new System.Drawing.Size(75, 23);
            this.AddBtn.TabIndex = 32;
            this.AddBtn.Text = "Add";
            this.AddBtn.UseVisualStyleBackColor = true;
            this.AddBtn.Click += new System.EventHandler(this.AddBtn_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(458, 80);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(37, 16);
            this.label18.TabIndex = 30;
            this.label18.Text = "Stage";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.StagedCount);
            this.panel1.Controls.Add(this.UnstageCount);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.DataGridStaged);
            this.panel1.Controls.Add(this.DataGridUnstaged);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(-2, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(438, 366);
            this.panel1.TabIndex = 0;
            // 
            // StagedCount
            // 
            this.StagedCount.AutoSize = true;
            this.StagedCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StagedCount.ForeColor = System.Drawing.Color.ForestGreen;
            this.StagedCount.Location = new System.Drawing.Point(279, 44);
            this.StagedCount.Name = "StagedCount";
            this.StagedCount.Size = new System.Drawing.Size(14, 13);
            this.StagedCount.TabIndex = 6;
            this.StagedCount.Text = "0";
            // 
            // UnstageCount
            // 
            this.UnstageCount.AutoSize = true;
            this.UnstageCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UnstageCount.ForeColor = System.Drawing.Color.Red;
            this.UnstageCount.Location = new System.Drawing.Point(73, 44);
            this.UnstageCount.Name = "UnstageCount";
            this.UnstageCount.Size = new System.Drawing.Size(14, 13);
            this.UnstageCount.TabIndex = 5;
            this.UnstageCount.Text = "0";
            this.UnstageCount.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(226, 41);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 16);
            this.label11.TabIndex = 4;
            this.label11.Text = "Staged : ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 41);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 16);
            this.label10.TabIndex = 3;
            this.label10.Text = "Unstage : ";
            // 
            // DataGridStaged
            // 
            this.DataGridStaged.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridStaged.Location = new System.Drawing.Point(222, 60);
            this.DataGridStaged.Name = "DataGridStaged";
            this.DataGridStaged.Size = new System.Drawing.Size(202, 300);
            this.DataGridStaged.TabIndex = 2;
            this.DataGridStaged.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridStaged_CellContentClick);
            // 
            // DataGridUnstaged
            // 
            this.DataGridUnstaged.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridUnstaged.Location = new System.Drawing.Point(12, 60);
            this.DataGridUnstaged.Name = "DataGridUnstaged";
            this.DataGridUnstaged.Size = new System.Drawing.Size(202, 300);
            this.DataGridUnstaged.TabIndex = 1;
            this.DataGridUnstaged.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridUnstaged_CellContentClick);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel2.Controls.Add(this.repoNameTxt);
            this.panel2.Controls.Add(this.label19);
            this.panel2.Controls.Add(this.branchlbl);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Location = new System.Drawing.Point(-2, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(440, 32);
            this.panel2.TabIndex = 0;
            // 
            // branchlbl
            // 
            this.branchlbl.AutoSize = true;
            this.branchlbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.branchlbl.ForeColor = System.Drawing.Color.Red;
            this.branchlbl.Location = new System.Drawing.Point(75, 9);
            this.branchlbl.Name = "branchlbl";
            this.branchlbl.Size = new System.Drawing.Size(44, 13);
            this.branchlbl.TabIndex = 1;
            this.branchlbl.Text = "master";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(11, 9);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(66, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "BRANCH: ";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.White;
            this.tabPage3.Controls.Add(this.label13);
            this.tabPage3.Controls.Add(this.label12);
            this.tabPage3.Controls.Add(this.UpstreamTxt);
            this.tabPage3.Controls.Add(this.DownstreamTxt);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(777, 366);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Tracker";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft PhagsPa", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(340, 29);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(126, 17);
            this.label13.TabIndex = 3;
            this.label13.Text = "Upstream Commits";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft PhagsPa", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(15, 29);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(145, 17);
            this.label12.TabIndex = 2;
            this.label12.Text = "Downstream Commits";
            // 
            // UpstreamTxt
            // 
            this.UpstreamTxt.ForeColor = System.Drawing.Color.ForestGreen;
            this.UpstreamTxt.Location = new System.Drawing.Point(334, 48);
            this.UpstreamTxt.Name = "UpstreamTxt";
            this.UpstreamTxt.Size = new System.Drawing.Size(299, 298);
            this.UpstreamTxt.TabIndex = 1;
            this.UpstreamTxt.Text = "";
            // 
            // DownstreamTxt
            // 
            this.DownstreamTxt.ForeColor = System.Drawing.Color.Red;
            this.DownstreamTxt.Location = new System.Drawing.Point(15, 48);
            this.DownstreamTxt.Name = "DownstreamTxt";
            this.DownstreamTxt.Size = new System.Drawing.Size(299, 298);
            this.DownstreamTxt.TabIndex = 0;
            this.DownstreamTxt.Text = "";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(187, 10);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(95, 13);
            this.label19.TabIndex = 2;
            this.label19.Text = "REPOSITORY: ";
            // 
            // repoNameTxt
            // 
            this.repoNameTxt.AutoSize = true;
            this.repoNameTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.repoNameTxt.ForeColor = System.Drawing.Color.Red;
            this.repoNameTxt.Location = new System.Drawing.Point(281, 10);
            this.repoNameTxt.Name = "repoNameTxt";
            this.repoNameTxt.Size = new System.Drawing.Size(32, 13);
            this.repoNameTxt.TabIndex = 3;
            this.repoNameTxt.Text = "repo";
            // 
            // panel4
            // 
            this.panel4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel4.BackgroundImage")));
            this.panel4.Location = new System.Drawing.Point(0, 23);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(810, 432);
            this.panel4.TabIndex = 4;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(558, 42);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(80, 16);
            this.label20.TabIndex = 4;
            this.label20.Text = "CONTROL";
            // 
            // GitControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(810, 450);
            this.Controls.Add(this.Management);
            this.Controls.Add(this.Tootls);
            this.Controls.Add(this.panel4);
            this.MainMenuStrip = this.Tootls;
            this.Name = "GitControl";
            this.Text = "GitControl";
            this.Tootls.ResumeLayout(false);
            this.Tootls.PerformLayout();
            this.Management.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.EmulatorPanel.ResumeLayout(false);
            this.EmulatorPanel.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridStaged)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridUnstaged)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip Tootls;
        private System.Windows.Forms.ToolStripMenuItem OpenCloneBox;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.TabControl Management;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox Local_dir_txt;
        private System.Windows.Forms.TextBox GIT_Link_txt;
        private System.Windows.Forms.TextBox GIT_dir_txt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Local_repo_dir_txt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel EmulatorPanel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox CmdTxt;
        private System.Windows.Forms.Button ExecuteBtn;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RichTextBox EmulatorResult;
        private System.Windows.Forms.ToolStripMenuItem RefreshTool;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label branchlbl;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridView DataGridStaged;
        private System.Windows.Forms.DataGridView DataGridUnstaged;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label StagedCount;
        private System.Windows.Forms.Label UnstageCount;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button RevertBtn;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox RevertTxt;
        private System.Windows.Forms.TextBox commitTxt;
        private System.Windows.Forms.TextBox addText;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button PushBtn;
        private System.Windows.Forms.Button CommitBtn;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button AddBtn;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.RichTextBox UpstreamTxt;
        private System.Windows.Forms.RichTextBox DownstreamTxt;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label repoNameTxt;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label20;
    }
}