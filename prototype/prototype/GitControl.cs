﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace prototype
{
    public partial class GitControl : Form
    {
        GIT_BL BL = new GIT_BL();
        public GitControl()
        {
            InitializeComponent();
            InitializeGlobalParams();


        }
        public void InitializeGlobalParams()
        {

            //initialize setting values
           
            if (BL.get_GIT_dir() != "")
            {
                GIT_dir_txt.Text = BL.get_GIT_dir();
            }
            if (BL.get_GIT_Link() != "")
            {
               
                GIT_Link_txt.Text = BL.get_GIT_Link();

            }
            if (BL.get_Local_clone_dir() != "")
            {
               
                Local_dir_txt.Text = BL.get_Local_clone_dir();

            }
            if (BL.get_Local_Dir_repo() != "")
            {
                Local_repo_dir_txt.Text = BL.get_Local_Dir_repo();
            }
        }

        private void OpenCloneBox_Click(object sender, EventArgs e)
        {
            CloneBox cloneForm = new CloneBox();
            cloneForm.FormClosed += new FormClosedEventHandler(child_FormClosed);
            cloneForm.Show(); 

            
        }
        void child_FormClosed(object sender, FormClosedEventArgs e)
        {
            //process form close
            RefreshTool_Click(sender, e);
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Settings settingForm = new Settings();
            settingForm.FormClosed += new FormClosedEventHandler(child_FormClosed);
            settingForm.Show();
        }


      

        private void ExecuteBtn_Click(object sender, EventArgs e)
        {
            EmulatorResult.Text = string.Empty;
            ExecuteBtn.Enabled = false;

            EmulatorResult.Text = BL.TerminalExecution(CmdTxt.Text.ToString());

            ExecuteBtn.Enabled = true;
            RefreshTool_Click(sender, e);
        }

        private void RefreshTool_Click(object sender, EventArgs e)
        {

            InitializeGlobalParams();
            string ValResult = BL.ValidateDirectory().Substring(0, 4).TrimEnd();
            if (ValResult == "true")
            {
                ViewStaged();
                ViewUnstaged();
                ViewBranch();
                Repository();
                ListUpstreamCommits();
                ListDownstreamCommits();
            }
        }


        private void ViewStaged()
        {
            List<string> output = new List<string>();
            output = BL.ListStaged();
            DataGridStaged.DataSource = output.Where(x => x != "").Select((x, index) =>
            new { Files = x }).OrderByDescending(x => x.Files).ToList();
            StagedCount.Text = DataGridStaged.RowCount.ToString();
        }
        private void ViewUnstaged()
        {
            List<string> output = new List<string>();
            output = BL.ListUnstaged();
            DataGridUnstaged.DataSource = output.Where(x => x != "").Select((x, index) =>
            new { Files = x }).OrderByDescending(x => x.Files).ToList();
            UnstageCount.Text = DataGridUnstaged.RowCount.ToString();
        }

        private void PushBtn_Click(object sender, EventArgs e)
        {
            string branch = branchlbl.Text.ToString();
            string result = BL.Push(branch);
            MessageBox.Show(result);
            RefreshTool_Click(sender, e);
            DialogResult res = MessageBox.Show("Pull Latest in branch " + branchlbl.Text, "Git Pull",
            MessageBoxButtons.OKCancel);
            switch (res)
            {
                case DialogResult.OK:
                    {
                        //this.Text = "[OK]";
                        MessageBox.Show(BL.Pull(branchlbl.Text));
                        break;

                    }
                case DialogResult.Cancel:
                    {
                        //this.Text = "[Cancel]";
                        break;
                    }
            }
        }

        private void DataGridUnstaged_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string Selected = "";
           
            Selected = DataGridUnstaged.CurrentRow.Cells[0].Value.ToString();
            addText.Text = Selected;
        }

        private void DataGridStaged_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string Selected = "";
           
            Selected = DataGridStaged.CurrentRow.Cells[0].Value.ToString();
            RevertTxt.Text = Selected;
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            BL.Add(addText.Text.ToString());
            addText.Text = "";
            RefreshTool_Click(sender, e);
        }

        private void RevertBtn_Click(object sender, EventArgs e)
        {
            BL.Revert(RevertTxt.Text.ToString());
            RevertTxt.Text = "";
            RefreshTool_Click(sender, e);
        }
        private void ViewBranch()
        {
            branchlbl.Text = BL.getBranch();
        }
        private void Repository()
        {
            string[] dir = Local_repo_dir_txt.Text.ToString().Split('\\');
            string repo = dir[dir.Length - 1].ToString().Replace(".", "").ToString();
            repoNameTxt.Text = repo;
        }
      
        private void CommitBtn_Click(object sender, EventArgs e)
        {
            string message = commitTxt.Text.ToString();
            string result = BL.Commit(message);
            MessageBox.Show(result);
            RefreshTool_Click(sender, e);
        }
        private void ListUpstreamCommits()
        {
            string list = BL.UpstreamCommits();
            UpstreamTxt.Text = list;
        }
        private void ListDownstreamCommits()
        {
            string list = BL.DownstreamCommits();
            DownstreamTxt.Text = list;
        }
       
    }
}
