﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace prototype
{
    public partial class Settings : Form
    {
        GIT_BL BL = new GIT_BL();
        string origLocalRepo = string.Empty;
        public Settings()
        {
            InitializeComponent();
           
            string GIT_DIR = ConfigurationSettings.AppSettings["GIT_DIR"];
            //initialize setting values
            if (GIT_DIR != "")
            {
                GIT_dir_txt.Text = GIT_DIR;
            }

            if ( BL.get_GIT_Link() != "")
            {
                // BL.set_GIT_Link(git_link);
                GIT_Link_txt.Text = BL.get_GIT_Link();
            }

            if ( BL.get_Local_clone_dir() != "")
            {
                //BL.set_Local_clone_dir(local_clone_dir);
                Local_dir_txt.Text = BL.get_Local_clone_dir();
            }

            if ( BL.get_Local_Dir_repo() != "")
            {
                Local_repo_dir_txt.Text = BL.get_Local_Dir_repo();
                origLocalRepo = Local_repo_dir_txt.Text;
            }

        }

        private void label1_Click(object sender, EventArgs e)
        {
            string path = GIT_dir_txt.Text.ToString();
            BL.FileViewer(ref path);
            GIT_dir_txt.Text = path;
            //BL.set_GIT_dir(path);
        }

        private void label3_Click(object sender, EventArgs e)
        {  
            string path = Local_dir_txt.Text.ToString();
            BL.FileViewer(ref path);
            Local_dir_txt.Text = path;
       
        }

       

        private void label6_Click(object sender, EventArgs e)
        {
            
            string path = Local_repo_dir_txt.Text.ToString();
            BL.FileViewer(ref path);
            Local_repo_dir_txt.Text = path;

        }

        private void SaveSettingsBtn_Click(object sender, EventArgs e)
        {
            string p1, p2, p3, p4;
            //BL.set_GIT_dir(GIT_dir_txt.Text.ToString());
           
            //BL.set_GIT_Link(GIT_Link_txt.Text.ToString());
            
            //BL.set_Local_clone_dir(Local_dir_txt.Text.ToString());
           
            //Set local dir to config temporarily 
            BL.set_Local_Dir_repo(Local_repo_dir_txt.Text.ToString());

            // p1 = BL.get_GIT_dir();

            //  p2 = BL.get_GIT_Link();

            //  p3 = BL.get_Local_clone_dir();

            // p4 = BL.get_Local_Dir_repo();

            //check if local dir is linked to git
            string ValResult = BL.ValidateDirectory().Substring(0,4).TrimEnd();
            if(ValResult != "true")
            {

               // MessageBox.Show("Directory ("+ Local_repo_dir_txt.Text.ToString() + ") is not linked to any GIT repository");
               //return the original config
               BL.set_Local_Dir_repo(origLocalRepo);
               Local_repo_dir_txt.Text = origLocalRepo;
                if(Local_repo_dir_txt.Text == "")
                {
                    SaveSettings();
                }
            }
            else
            {
                SaveSettings();
            }
            
            //if(p1 == GIT_dir_txt.Text.ToString() && p2 == GIT_Link_txt.Text.ToString() && p3 == Local_dir_txt.Text.ToString() && p4 == Local_repo_dir_txt.Text.ToString())
            //{
            //   // MessageBox.Show("Changes Saved!");
            //}
            

        }
        private void SaveSettings()
        {
            BL.set_GIT_dir(GIT_dir_txt.Text.ToString());

            BL.set_GIT_Link(GIT_Link_txt.Text.ToString());

            BL.set_Local_clone_dir(Local_dir_txt.Text.ToString());

            BL.set_Local_Dir_repo(Local_repo_dir_txt.Text.ToString());
            MessageBox.Show("Changes Saved!");
        }
    }
}
