﻿namespace prototype
{
    partial class CloneBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CloneBtn = new System.Windows.Forms.Button();
            this.CloneResult = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Link_Txt = new System.Windows.Forms.TextBox();
            this.Local_Dir_txt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // CloneBtn
            // 
            this.CloneBtn.Location = new System.Drawing.Point(344, 75);
            this.CloneBtn.Name = "CloneBtn";
            this.CloneBtn.Size = new System.Drawing.Size(75, 23);
            this.CloneBtn.TabIndex = 0;
            this.CloneBtn.Text = "Clone";
            this.CloneBtn.UseVisualStyleBackColor = true;
            this.CloneBtn.Click += new System.EventHandler(this.CloneBtn_Click);
            // 
            // CloneResult
            // 
            this.CloneResult.Location = new System.Drawing.Point(12, 110);
            this.CloneResult.Name = "CloneResult";
            this.CloneResult.ReadOnly = true;
            this.CloneResult.Size = new System.Drawing.Size(418, 96);
            this.CloneResult.TabIndex = 1;
            this.CloneResult.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Git Link";
            // 
            // Link_Txt
            // 
            this.Link_Txt.Location = new System.Drawing.Point(94, 17);
            this.Link_Txt.Name = "Link_Txt";
            this.Link_Txt.Size = new System.Drawing.Size(326, 20);
            this.Link_Txt.TabIndex = 3;
            // 
            // Local_Dir_txt
            // 
            this.Local_Dir_txt.Location = new System.Drawing.Point(93, 48);
            this.Local_Dir_txt.Name = "Local_Dir_txt";
            this.Local_Dir_txt.Size = new System.Drawing.Size(326, 20);
            this.Local_Dir_txt.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Local Dirctory";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel1.Controls.Add(this.Local_Dir_txt);
            this.panel1.Controls.Add(this.CloneBtn);
            this.panel1.Controls.Add(this.Link_Txt);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(-3, -2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(516, 106);
            this.panel1.TabIndex = 6;
            // 
            // CloneBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(444, 214);
            this.Controls.Add(this.CloneResult);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(460, 252);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(460, 252);
            this.Name = "CloneBox";
            this.Text = "CloneBox";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.do_when_close);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button CloneBtn;
        private System.Windows.Forms.RichTextBox CloneResult;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Link_Txt;
        private System.Windows.Forms.TextBox Local_Dir_txt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
    }
}