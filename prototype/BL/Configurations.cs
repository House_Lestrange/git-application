﻿using prototype;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;
using System.Windows.Forms;

namespace prototype
{
    //public class Configurations
    //{
    //    public string Local_Dir_repo { get; set; }
    //    public string Local_clone_dir { get; set; }
    //    public string GIT_Link { get; set; }

    //}
    
    public class GIT_BL
    {
        //Configurations conf = new Configurations();
        string GIT_DIR = ConfigurationSettings.AppSettings["GIT_DIR"];
        int[][] myaaray;
        int[,] myaray;

        public string GIT_Bridge(string Command, string param)
        {
            string res = string.Empty;
            try
            {
                //string GIT_DIR = string.Empty;
                //Get GIT Directory
               
                ProcessStartInfo gitInfo = new ProcessStartInfo();
                gitInfo.CreateNoWindow = true;
                gitInfo.UseShellExecute = false;
                gitInfo.RedirectStandardError = true;
                gitInfo.RedirectStandardOutput = true;
                gitInfo.FileName = @GIT_DIR;
                //create a Process to actually run the command.
                Process gitProcess = new Process();
                gitInfo.Arguments = Command + " " + param;

                //Get the working dir
                string Target_DIR = String.Empty;
                if (Command == "clone")
                {
                    Target_DIR = get_Local_clone_dir();//conf.Local_clone_dir;
                    
                }
                else
                {
                    Target_DIR = get_Local_Dir_repo();//conf.Local_Dir_repo;
                }
                gitInfo.WorkingDirectory = @Target_DIR;

                //Start Process
                gitProcess.StartInfo = gitInfo;
                gitProcess.Start();
                List<string> output = new List<string>();
                string stderr_str = gitProcess.StandardError.ReadToEnd();
                string stdout_str = gitProcess.StandardOutput.ReadToEnd();
                //Print/Return possible outputs
                res = stdout_str + " " + stderr_str;

                //End Process
                gitProcess.WaitForExit();
                gitProcess.Close();
            }
            catch (Exception e)
            {
                res = e.ToString();
            }
           
            
            return res;
        }
        ///SET GLOBAL PROPERTIES
        public void set_GIT_dir(string path)
        {
            // conf.Local_Dir_repo = path;
            ConfigurationSettings.AppSettings["GIT_DIR"] = path;
        }
        public void set_Local_Dir_repo(string path)
        {
            // conf.Local_Dir_repo = path;
            ConfigurationSettings.AppSettings["LOCAL_DIR_REPO"] = path;
        }
        public void set_Local_clone_dir(string path)
        {
            // conf.Local_clone_dir = path;
            ConfigurationSettings.AppSettings["LOCAL_CLONE_DIR"] = path;
        }
        public void set_GIT_Link(string path)
        {
            //conf.GIT_Link = path;
            ConfigurationSettings.AppSettings["GIT_LINK"] = path;
        }

        public string get_GIT_dir()
        {
            // conf.Local_Dir_repo = path;
            return ConfigurationSettings.AppSettings["GIT_DIR"];
        }
        public string get_Local_Dir_repo()
        {
            return ConfigurationSettings.AppSettings["LOCAL_DIR_REPO"];
        }
        public string get_Local_clone_dir()
        {
           return ConfigurationSettings.AppSettings["LOCAL_CLONE_DIR"];
        }
        public string get_GIT_Link()
        {
            return ConfigurationSettings.AppSettings["GIT_LINK"];
        }

        //METHODS
        public void FileViewer(ref string path)
        {
            
            FolderBrowserDialog folderDlg = new FolderBrowserDialog();
            folderDlg.ShowNewFolderButton = true;
            // Show the FolderBrowserDialog.
            DialogResult result = folderDlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                //Local_dir_txt.Text = folderDlg.SelectedPath;
                //BL.set_Local_clone_dir(folderDlg.SelectedPath);
                path = folderDlg.SelectedPath;
                Environment.SpecialFolder root = folderDlg.RootFolder;
            }
            
        }
        public string Clone()
        {
            string Clone_Result = String.Empty;
            try
            {
               
                string Command = "clone";
                string Link = get_GIT_Link();//conf.GIT_Link;
                string cloned_Dir = get_Local_clone_dir();//conf.Local_clone_dir;
                Clone_Result = GIT_Bridge(Command,Link);

                string[] clonedFile = Link.ToString().Split('/');

                string file = clonedFile[clonedFile.Length - 1].ToString().Replace(".", "").ToString();
                string fullpath = cloned_Dir + "\\" + file.Substring(0,file.Length-3);
                if (Directory.Exists(fullpath))
                {
                    Clone_Result += "\n" + "Success: " + " Cloned to local directory:" + "\n" + fullpath + "\n" + "Cloned: " + file.Substring(0, file.Length - 3);
                    set_Local_Dir_repo(fullpath);
                  
                }
                else
                {
                    Clone_Result += "\n" + "Failed: " + " Cloning to local directory:" + "\n" + fullpath + "\n";
                }

            }
            catch (Exception e)
            {
                Clone_Result = e.ToString();
            }
            return Clone_Result;
        }
        public string TerminalExecution(string command)
        {
            string result = string.Empty;
            try
            {
                string Command = command;
                string Link = get_GIT_Link();//conf.GIT_Link;
                string cloned_Dir = get_Local_clone_dir();//conf.Local_clone_dir;
                result = GIT_Bridge(Command, "");
            }
            catch (Exception e)
            {

                result = e.ToString();
            }
            return result;
        }
        public List<string> ListStaged ()
        {
            List<string> output = new List<string>();
            try
            {
                
                string command = "diff --name-only --cached";
                string res = GIT_Bridge(command, "");
                string[] list = res.Split('\n');
                foreach (var item in list)
                {
                    if (item != "" && item != " ")
                    {
                        output.Add(item);
                        
                    }
                }
            }
            catch (Exception e)
            {

                output = null;
            }
            return output;
        }
        public List<string> ListUnstaged()
        {
            
            List<string> output = new List<string>();
            try
            {

                string command = "ls-files --others --modified";
                string res = GIT_Bridge(command, "");
                string[] list = res.Split('\n');
                foreach (var item in list)
                {
                    if (item != "" && item != " ")
                    {
                        output.Add(item);

                    }
                }
            }
            catch (Exception e)
            {

                output = null;
            }
            return output;
        }
        public string Add(string item)
        {
            string result = string.Empty;
            try
            {
                string command = "add";
                result = GIT_Bridge(command, "\"" + item + "\"");
            }
            catch (Exception e)
            {

                result = e.ToString();
            }
            return result;
        }
        public string Revert(string item)
        {
            string result = string.Empty;
            try
            {
                string command = "rm --cached";
                result = GIT_Bridge(command, "\"" + item + "\"");
            }
            catch (Exception e)
            {

                result = e.ToString();
            }
            return result;
        }
        public string getBranch()
        {
            string result = string.Empty;
            try
            {
                string command = "branch";
                result = GIT_Bridge(command, "");
                List<string> output = new List<string>();
                string[] list = result.Split('\n');
                foreach (var item in list)
                {
                    if (item != "")
                    {
                        if (item.Contains("*"))
                        {
                            output.Add(item);
                        }


                    }
                }
                result = output[0].Trim().Replace(" ", "").Replace("*", "");
            }
            catch (Exception e)
            {
                result = e.ToString();
            }
            return result;
        }
       public string Commit(string message)
        {
            string result = string.Empty;
            message = "\"" + message + "\"";
            try
            {
                result = GIT_Bridge("commit -m", message);
            }
            catch (Exception e)
            {

                result = e.ToString();
            }
            return result;
        }
        public string Push(string branch)
        {
            string result = string.Empty;
           
            try
            {
                result = GIT_Bridge("push origin", branch);
            }
            catch (Exception e)
            {

                result = e.ToString();
            }
            return result;
        }
        public string Pull(string branch)
        {

            string result = string.Empty;

            try
            {
                result = GIT_Bridge("pull origin", branch);
            }
            catch (Exception e)
            {

                result = e.ToString();
            }
            return result;
        }
        public string DownstreamCommits()
        {
             string result = string.Empty;
            try
            {
                result = GIT_Bridge("cherry", "-v");
            }
            catch (Exception e)
            {

                result = e.ToString();
            }
            return result;
        }
        public string UpstreamCommits()
        {
            
             string result = string.Empty;
            try
            {
                result = GIT_Bridge("log","@{u}");
            }
            catch (Exception e)
            {

                result = e.ToString();
            }
            return result;
        }
        public string ValidateDirectory()
        {
            string command = "rev-parse --is-inside-work-tree";
            string result = GIT_Bridge(command, "");
            return result;
        }
    }
}
